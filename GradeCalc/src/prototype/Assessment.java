package prototype;

public class Assessment {
	
	public String name;
	public int overallWeighting;
	
	public int numOfParts;
	private int[] marks;
	public double[] weightings;
	
	private double totalGrade; 
	
	// Create Assessment with a particular number of parts
	public Assessment(String name, int numOfParts, int overallWeighting) {
		this.name = name;
		this.numOfParts = numOfParts;
		this.marks = new int[numOfParts];
		this.overallWeighting = overallWeighting;
		
		// Assume that the weightings for each part are equal
		this.weightings = new double[numOfParts];
		for (int i = 0; i < numOfParts; i++) {
			this.weightings[i] = overallWeighting / numOfParts;
		}
		
		this.totalGrade = 0;
	}
	
	// Get all the marks
	public int[] getMarks() {
		return this.marks;
	}
	
	// Set one of the marks
	// TODO Allow user to add in their mark's potential (13/20 => 20)
	// TODO Store that info
	public void setMark(int index, int mark, int weighting) {
		
		// checks if the given weighting will cause errors
		if (!checkWeightingError(index, weighting)) {
			System.err.println("Weighting is too high/low");
			return;
		}
		
		this.marks[index] = mark;
		this.weightings[index] = weighting;
		
		// set the rest of the weightings to -1
		// because they are now invalid
		for (int i = index+1; i < numOfParts; i++) {
			this.weightings[i] = -1;
		}
		
		// Update totalGrade
		updateTotalGrade(mark);
	}
	
	public void updateTotalGrade(int mark) {
		this.totalGrade += mark;
	}
	
	public boolean checkWeightingError(int index, int weighting) {
		double sum = weighting;
		for (int i = 0; i < index; i++) {
			sum += this.weightings[i];
		}
		
		if (sum <= overallWeighting+0.0001f || sum >= overallWeighting-0.0001f) {
			return true;
		} else {
			return false;
		}
	}
	
	public double getTotalGrade(){
		return totalGrade;
	}
}
