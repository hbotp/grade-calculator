package prototype;

public class Subject {

	public String name;
	public int numOfAssessments;
	public Assessment[] assessments;
	
	// Creates a Subject with a certain number of Assessments
	public Subject(String name, int numOfAssessments) {
		this.name = name;
		this.numOfAssessments = numOfAssessments;
		this.assessments = new Assessment[numOfAssessments];
	}
	
	// TODO calculate current grade and required grade for HD, D, etc
}

